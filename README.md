# sdiffengine

inspired by a certain entrance exam, a diff engine to figure leading sequence numbers dynamically

# install

```sh
make
make install
```

# usage

```sh
usage: sdiffengine [-vb] [-f file] [-n continuation]
       -f ... reads from stdin if not specified
       -n ... total n desired sequence numbers
              set to 1 if not specified
       -b ... verbose output
       -v ... print version
```

# results

### test sequence cases

##### additive sequences (+1, +1, +1, ...)

`0 1 2 3 4 5 6` => `0 1 2 3 4 5 6 7`

correctly recognizes the pattern

##### multiplicative sequences (*2, *3, *4, ...)

`1 2 6 24` => `1 2 6 24 120`

correctly recognizes the pattern

##### mixed sequences (+(1*6), +(2*6), +(3*6), ...)

`1 7 19 37` => `1 7 19 37 61`

correctly recognizes the pattern

##### alternating sequences *2, /2, *2, ...

`2 4 2 4 2 4` => `2 4 2 4 2 4 2`

correctly recognizes the pattern

##### bogus sequences

`1 420 69 5555 9001` => no solution

correctly recognizes *no* pattern

### fibonicci series

the puzzle sequences on fibonicci were fun, but I find myself pondering how many of them have a solution that can be found using this primitive approach

##### https://www.fibonicci.com/numerical-reasoning/number-sequences-test/easy/ results

|stat|res|
|:--|--:|
|correct  |7|
|incorrect|1|
|total    |8|

##### https://www.fibonicci.com/numerical-reasoning/number-sequences-test/hard/ results

|stat|res|
|:--|--:|
|correct  |8|
|incorrect|13|
|total    |21|

##### https://www.fibonicci.com/numerical-reasoning/number-sequences-test/medium/ results

|stat|res|
|:--|--:|
|correct  |10|
|incorrect|3|
|total    |13|

