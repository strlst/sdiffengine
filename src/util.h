#include "def.h"

/* helper print functions for custom data structure */
void print_diff_layer(diff_layer*);
void print_diff_layer_seq(diff_layer*);

/* respecc verbose flag */
extern char *opt_verb;
int verbose(const char * restrict format, ...);
int fverbose(FILE*, const char * restrict format, ...);
