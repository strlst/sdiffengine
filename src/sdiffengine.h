#include "def.h"

#include "arg.h"
#include "sde.h"
#include "util.h"

/* global flags */
char *argv0;
char *opt_verb = NULL;
static char *opt_file = NULL;
static char *opt_size = NULL;

void die(const char*, ...);
void usage(void);
int main(int, char**);
