#include "def.h"
#include "util.h"

/* start operation */
diff_layer *gen_initial_seq(FILE*);

/* diff layer operation functions */
diff_layer *create_diff_layer(double*, size_t, int, char*);
void destroy_diff_layer(diff_layer*);
int validate_consistency(diff_layer*, int);
char *gen_type(char*, char);

/* diff generation */
int gen_diffs(diff_layer*);
int gen_all_diffs(diff_layer*);
void continue_seq(diff_layer*, diff_layer*, size_t, double (*operation)(double,double));

/* 'generic' operation functions */
double copy_prev(double, double);
double add_prev(double, double);
double multiply_prev(double, double);

/* sequence generation */
void gen_next(diff_layer*, double*);
int gen_next_n(diff_layer*, size_t);
