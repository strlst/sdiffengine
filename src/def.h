#include "stdlib.h"
#include "stdio.h"
#include "stdarg.h"
#include "limits.h"
#include "assert.h"
#include "math.h"

/* constants? in my program? */
#define BUF_MAX 1024
#define LAYERS 32
#define ROOT_LAYER 0

/* used for verbose output */
#define VERBOSE   "AAA"
#define ROOT_TYPE 'R'
#define ADD_TYPE  'A'
#define MUL_TYPE  'M'

/* a sequence can either be consistent or inconsistent */
/* the values are used during examination of differential sequences, */
/* that are calculated from an initial sequence */
/* if a differential sequence is consistent, it means a representable */
/* pattern has been found that can be used to reconstruct a possible */
/* sequence continuation */
#define INCONSISTENT  0
#define CONSISTENT    1

/* underlying mathematical principle that enables this type of */
/* sequence guessing, represented as definitions, sort of */
/* success return codes */
#define NOTHING_FOUND   INCONSISTENT
#define SOLUTION_FOUND  CONSISTENT

/* error return codes */
#define ERR_SEQUENCE_EXHAUSTED -1
#define ERR_MAX_DEPTH_REACHED  -2

#ifndef DIFF_LAYER_DEF
#define DIFF_LAYER_DEF

/* datastructure (tree) containing: */
/* - pointer to a (double[]) sequence */
/* - metadata pertaining to sequence */
/* - consistency rating */
/* - child nodes for differential sequences */
typedef struct diff_layer {
    struct diff_layer *a_delta;
    struct diff_layer *m_delta;
    double *seq;
    size_t size;
    int depth;
    char *type;
    int consistency;
} diff_layer;

#endif
