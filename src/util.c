#include "util.h"

/* takes a diff layer and prints a passable verbose representation */
void print_diff_layer(diff_layer *dl) {
    if (dl == NULL)
        return;

    verbose("%s%i (%i C): ", dl->type, dl->depth, dl->consistency);
    for (int i = 0; i < dl->size - dl->depth; i++)
        verbose("%f ", dl->seq[i]);
    verbose("\n");
}

/* takes a diff layer and prints a passable representation */
void print_diff_layer_seq(diff_layer *dl) {
    if (dl == NULL)
        return;

    for (int i = 0; i < dl->size - 1; i++)
        printf("%g ", dl->seq[i]);
    printf("%g\n", dl->seq[dl->size - 1]);
}

/* same as 'verbose(...)', but for custom file pointer */
int fverbose(FILE* file, const char * restrict format, ...) {
    if(opt_verb == NULL)
        return 0;

    va_list args;
    va_start(args, format);
    int ret = vfprintf(file, format, args);
    va_end(args);

    return ret;
}

/* takes same parameters as printf, but outputs only on verbose flag */
int verbose(const char * restrict format, ...) {
    if(opt_verb == NULL)
        return 0;

    va_list args;
    va_start(args, format);
    int ret = vprintf(format, args);
    va_end(args);

    return ret;
}
