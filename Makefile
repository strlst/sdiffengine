CFLAGS:=
CXX:= gcc
PROG:= sdiffengine

OBJS:= util.o sde.o sdiffengine.o

all: $(PROG)

%.o: src/%.c src/%.h src/def.h
	$(CXX) $(CFLAGS) -c $<

$(PROG): $(OBJS)
	$(CXX) -Wall -std=c99 -pedantic -g $(OBJS) -o $(PROG) $(LDFLAGS)
	rm *.o

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${PROG} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${PROG}

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/${PROG}

clean:
	rm $(PROG) *.o